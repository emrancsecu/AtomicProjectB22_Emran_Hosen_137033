<?php
namespace Bitm\SEIP137033\Birthday;
namespace App\Bitm\SEIP137033\Birthday;
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Utility\Utility;
use App\Bitm\SEIP137033\Message\Message;

class Birthday{
    public $id="";
    public $name="";
    public $date="";
    public $conn;
    public $deleted_at;

    public function prepare($data=""){
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists("name",$data)){
            $this->name=$data['name'];
        }

        if(array_key_exists("date",$data)){
            $this->date=$data['date'];
        }
    }

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectb22") or die("Database Connection Failed");
    }

    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`birthday` (`name`,`date`) VALUES ('".$this->name."','".$this->date."')";
//        echo $query;
//        die();
        $result=mysqli_query($this->conn,$query);
        if($result){
            Message::message("
            <div class=\"alert alert-success\">
              <strong>Success!</strong> Data has been stored successfully.
            </div>");
            // echo "Data Sent Succesfully";
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }

    }

    public function index(){
        $_allDate= array();
        $query="SELECT * FROM `birthday` WHERE `deleted_at` IS NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allDate[]=$row;
        }
        return $_allDate;
    }

    public function view(){
        $query="SELECT * FROM `birthday` WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_object($result);
        return $row;

    }

    public function update(){
        $query="UPDATE `birthday` SET `name` = '". $this->name ."', `date` = '".$this->date."' WHERE `birthday`.`id` = ".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result){
            Message::message("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::redirect("index.php");
        }
        else {
            echo "Error";
        }
    }

    public function delete(){
        $query="DELETE FROM `atomicprojectb22`.`birthday` WHERE `birthday`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect("index.php");
        }
    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicprojectb22`.`birthday` SET `deleted_at` =" . $this->deleted_at . " WHERE `birthday`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been trashed successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been trashed successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed(){
        $_allDate= array();
        $query="SELECT * FROM `birthday` WHERE `deleted_at` IS NOT NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allDate[]=$row;
        }
        return $_allDate;
    }

    public function recover(){
        $this->deleted_at=time();
        $query="UPDATE `birthday` SET `deleted_at` = NULL WHERE `birthday`.`id` =".$this->id;
//        echo $query;
//        die();
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been recovered successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been recovered successfully.
</div>");
            Utility::redirect("index.php");
        }
    }

    public function recoverSelected($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicprojectb22`.`birthday` SET `deleted_at` = NULL WHERE `birthday`.`id` IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been recovered successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been recovered successfully.
</div>");
                Utility::redirect("index.php");
            }

        }

    }

    public function deleteSelected($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicprojectb22`.`birthday` WHERE `birthday`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been deleted successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been deleted successfully.
</div>");
                Utility::redirect("index.php");
            }

        }
    }


    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb22`.`birthday` WHERE deleted_at IS NULL";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom,$Limit){
        $_allDate = array();
        $query="SELECT * FROM `birthday` WHERE deleted_at IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allDate[] = $row;
        }

        return $_allDate;

    }





}



?>