<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Add Birthday Information</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>Add Birthday Information</h2>
    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label for="Title">Enter Your Name</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Your Name" required>
        </div>

        <div class="form-group">
            <label for="Title">Select Your Birthday</label>
            <input type="date" class="form-control" name="date" id="date"  required>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>