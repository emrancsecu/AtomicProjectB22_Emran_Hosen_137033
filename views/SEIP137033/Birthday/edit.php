<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Birthday\Birthday;
//use App\Bitm\SEIP137033\Utility\Utility;
//use App\Bitm\SEIP137033\Message\Message;

$birthday= new Birthday();
$birthday->prepare($_GET);
//Utility::dd($birthday);
//die();
$singleItem=$birthday->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Your Name and Birthday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>Edit Book title</h2>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label for="Name">Enter Your Name and Birthday</label>
            <input type="hidden" name="id"  value="<?php echo $singleItem->id?>">
            <input type="text" class="form-control" name="name" placeholder="Enter Your Name" value="<?php echo $singleItem->name?>" required>
            <input type="date" name="date" id="date" class="form-control" value="<?php echo $singleItem->date ?>">
        </div>
        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>
