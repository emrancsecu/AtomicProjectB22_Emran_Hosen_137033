<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Birthday\Birthday;
use App\Bitm\SEIP137033\Utility\Utility;
use App\Bitm\SEIP137033\Message\Message;

$birthday= new Birthday();


if(array_key_exists('itemPerPage',$_SESSION)) {
if (array_key_exists('itemPerPage', $_GET)) {
$_SESSION['itemPerPage'] = $_GET['itemPerPage'];
}
}
else{
$_SESSION['itemPerPage']=5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$birthday->count();

//echo $totalItem;
//die();



$totalPage=ceil($totalItem/$itemPerPage);
//Utility::dd($itemPerPage);
$pagination="";


if(array_key_exists('pageNumber',$_GET)){
$pageNumber=$_GET['pageNumber'];
}else{
$pageNumber=1;
}

$previous="";
if($pageNumber>1){
$prev=$pageNumber-1;
$previous="<li><a href='index.php?pageNumber=$prev'>&#10094; Previous</a></li>";
}

$next="";
if($pageNumber<$totalPage){

$next_item=$pageNumber+1;
$next="<li><a href='index.php?pageNumber=$next_item'>Next &#10095;</a></li>";

}


for($i=1;$i<=$totalPage;$i++){
$class=($pageNumber==$i)?"active":"";
$pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);
$allDate=$birthday->paginator($pageStartFrom,$itemPerPage);

//Utility::d($allDate);
//die();

?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php')?>
</head>
<body>

<div class="container">
    <h2>All Birthday List</h2>
    <a href="create.php" class="btn btn-primary" role="button">Create again</a>  <a href="trashed.php" class="btn btn-primary" role="button">View Trashed list</a>

    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
    </div>

    <form role="form">
        <div class="form-group">
            <label for="sel1">Select homw many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option <?php if($itemPerPage == 5) echo "selected" ?> >5</option>
                <option <?php if($itemPerPage == 10) echo "selected" ?> >10</option>
                <option <?php if($itemPerPage == 15) echo "selected" ?> >15</option>
                <option <?php if($itemPerPage == 20) echo "selected" ?> >20</option>
                <option <?php if($itemPerPage == 25) echo "selected" ?> >25</option>
                <option <?php if($itemPerPage == 30) echo "selected" ?> >30</option>
                <option <?php if($itemPerPage == 35) echo "selected" ?> >35</option>
                <option <?php if($itemPerPage == 40) echo "selected" ?> >40</option>
                <option <?php if($itemPerPage == 45) echo "selected" ?> >45</option>
                <option <?php if($itemPerPage == 50) echo "selected" ?> >50</option>
            </select>
            <button type="submit">Go!</button>

        </div>
    </form>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Serial</th>
                <th>ID</th>
                <th>Name</th>
                <th>Birthday</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allDate as $birthday){
                $sl++;

                $birthdate = $birthday->date;
                $time = strtotime($birthdate);
                $bdate = date("d/m/y", $time);

                ?>
                <td><?php echo $sl+$pageStartFrom ?></td>
                <td><?php echo $birthday->id?></td>
                <td><?php echo $birthday->name?></td>
                <td><?php echo $bdate?></td>
                <td><a href="view.php?id=<?php echo $birthday-> id ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $birthday-> id ?>"  class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $birthday->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                    <a href="trash.php?id=<?php echo $birthday->id?>"  class="btn btn-warning" role="button">Move ot Trash</a>
                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>


    <div class="text-center">

        <ul class="pagination">
            <?php echo $previous?>
            <?php echo $pagination?>
            <?php echo $next?>
        </ul>
    </div>




</div>
<script>
    $('#message').show().delay(2000).fadeOut();


    //        $(document).ready(function(){
    //            $("#delete").click(function(){
    //                if (!confirm("Do you want to delete")){
    //                    return false;
    //                }
    //            });
    //        });
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>
</body>
</html>


