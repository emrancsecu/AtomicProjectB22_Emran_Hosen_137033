<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Birthday\Birthday;
//use App\Bitm\SEIP137033\Utility\Utility;
//use App\Bitm\SEIP137033\Message\Message;

$birthday= new Birthday();
$birthday->prepare($_GET);
//Utility::dd($birthday);
//die();
$singleItem=$birthday->view();

$birthdate = $singleItem->date;
$time = strtotime($birthdate);
$bdate = date("d/m/y", $time);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>View ID, Name and Birthday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>View ID, Name and Birthday</h2>
    <ul class="list-group">

        <li class="list-group-item"><?php echo $singleItem->id;?></li>
        <li class="list-group-item"><?php echo $singleItem->name;?></li>

        <li class="list-group-item"><?php echo $bdate;?></li>
    </ul>

    <a href="index.php" class="btn btn-primary" role="button">Back To List</a>
</div>

</body>
</html>


