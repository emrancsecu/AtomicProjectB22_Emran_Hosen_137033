<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Create Book Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>Create Book title</h2>
    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label for="Title">Enter Book Title</label>
            <input type="text" class="form-control" name="title" id="title" placeholder="Enter Book Title" required>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>