<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Book\Book;
//use App\Bitm\SEIP137033\Utility\Utility;
//use App\Bitm\SEIP137033\Message\Message;

$book= new Book();
$book->prepare($_GET);
//Utility::dd($book);
//die();
$singleItem=$book->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Book Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>Edit Book title</h2>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label for="Title">Enter Book Title</label>
            <input type="hidden" name="id" id="title" value="<?php echo $singleItem->id?>">
            <input type="text" class="form-control" name="title" id="title" placeholder="Enter Book Title" value="<?php echo $singleItem->title?>" required>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
