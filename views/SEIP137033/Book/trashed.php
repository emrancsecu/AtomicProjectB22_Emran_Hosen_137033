<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Book\Book;
use App\Bitm\SEIP137033\Utility\Utility;
use App\Bitm\SEIP137033\Message\Message;

$book= new Book();
$allBook=$book->trashed();
//Utility::dd($allBook);

?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>All Trash Item List</h2>
    <form action="recoverselected.php" method="post" id="multiple">
    <a href="create.php" class="btn btn-primary" role="button">Create</a>
    <a href="index.php" class="btn btn-primary" role="button">Back To list</a>
    <button type="submit" class="btn btn-primary">Recover Selected</button>
    <button type="button" class="btn btn-primary" id="multiple_delete">Delete Selected</button>
    <br>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Check Item</th>
                <th>Serial</th>
                <th>ID</th>
                <th>Book Title</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach($allBook as $book){
                $sl++;
                ?>
                <tr>
                    <td><input type="checkbox" name="mark[]" value="<?php echo $book['id'] ?>"></td>
                    <td><?php echo $sl ?></td>
                    <td><?php echo $book['id'] ?></td>
                    <td><?php echo $book['title'] ?></td>
                    <td>
                        <a href="recover.php?id=<?php echo $book['id']?>" class="btn btn-primary" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo $book['id']?>" class="btn btn-danger" role="button" id="delete" Onclick="return ConfirmDelete()" >Delete</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        </form>
    </div>
</div>
<script>
    $('#message').show().delay(2000).fadeOut();


    $(document).ready(function(){
        $("#delete").click(function(){
            if (!confirm("Do you want to delete")){
                return false;
            }
        });
    });

    $('#multiple_delete').on('click',function(){
        document.forms[0].action="deleteselected.php";
        $('#multiple').submit();
    });

</script>
</body>
</html>
