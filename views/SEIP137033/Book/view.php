<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Book\Book;
//use App\Bitm\SEIP122479\Book\Utility;
//use App\Bitm\SEIP122479\Book\Message;

$book= new Book();
$book->prepare($_GET);
//Utility::dd($book);
//die();
$singleItem=$book->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Book Title and ID</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>View Book Title and ID</h2>
    <ul class="list-group">
        
        <li class="list-group-item"><?php echo $singleItem->id;?></li>
        
        <li class="list-group-item"><?php echo $singleItem->title;?></li>
    </ul>

    <a href="index.php" class="btn btn-primary" role="button">Back To List</a>
</div>

</body>
</html>


