<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Add City and Name </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2> Add Name and City</h2>
    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label for="Title">Enter Your Name</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Your Name" required>
        </div>

            <div class="form-group">
                <label for="sel1">Select Your City from the below list (select one):</label>
                <select class="form-control" id="sel1" name="city">
                    <option disabled selected>Select Your City</option>
                    <option>Dhaka</option>
                    <option>Gazipur</option>
                    <option>Mymensing</option>
                    <option>Chittagong</option>
                    <option>Rajshahi</option>
                    <option>Magura</option>
                    <option>Comilla</option>
                </select>
            </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>