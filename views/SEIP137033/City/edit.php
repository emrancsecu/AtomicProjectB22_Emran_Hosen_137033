<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\City\City;
//use App\Bitm\SEIP137033\Utility\Utility;
//use App\Bitm\SEIP137033\Message\Message;

$city= new City();
$city->prepare($_GET);
//Utility::dd($town);
//die();
$singleItem=$city->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Name and City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>Edit Name and City</h2>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label for="Name">Enter Your Name</label>
            <input type="hidden" name="id"  value="<?php echo $singleItem->id?>">
            <input type="text" class="form-control" name="name" placeholder="Enter Your Name" value="<?php echo $singleItem->name?>" required>
            <label for="sel1">Select Your City from the below list (select one):</label>
            <select class="form-control" id="sel1" name="city">
                <option disabled selected value="<?php echo $singleItem->city?>">Select Your City</option>
                <option>Dhaka</option>
                <option>Gazipur</option>
                <option>Mymensing</option>
                <option>Chittagong</option>
                <option>Rajshahi</option>
                <option>Magura</option>
                <option>Comilla</option>
            </select>

        </div>
        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>