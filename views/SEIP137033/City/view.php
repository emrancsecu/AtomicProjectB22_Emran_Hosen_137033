<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\City\City;
//use App\Bitm\SEIP122479\Book\Utility;
//use App\Bitm\SEIP122479\Book\Message;

$city= new City();
$city->prepare($_GET);
//Utility::dd($city);
//die();
$singleItem=$city->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Name and City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>View Name and City</h2>
    <ul class="list-group">

        <li class="list-group-item"><?php echo $singleItem->id;?></li>

        <li class="list-group-item"><?php echo $singleItem->name;?></li>

        <li class="list-group-item"><?php echo $singleItem->city;?></li>
    </ul>

    <a href="index.php" class="btn btn-primary" role="button">Back To List</a>
</div>

</body>
</html>


