<?php
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Create checkbox</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>Form control: checkbox</h2>
    
    <form role="form" method="post" action="store.php">
    <label>Name:</label>
    <input type="text" name="name" required>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Gardening">Gardening</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Playing Football">Playing Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Coding">Coding</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Reading">Reading</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Swimming">Swimming</label>
        </div>
        <input type="submit">
    </form>
</div>

</body>
</html>


