<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Hobby\Hobby;
use App\Bitm\SEIP137033\Message\Message;
use App\Bitm\SEIP137033\Utility\Utility;

$hobby= new Hobby();
$hobby->prepare($_GET);
$singleItem=$hobby->view();

$editItem=explode(",",$singleItem->hobbies);
//echo $singleItem->id;
//Utility::dd($singleItem);
//Utility::dd($editItem);
//die();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>Form control: checkbox</h2>
   
    <form role="form" method="post" action="update.php">
    <div>
    <label>Name:</label>
    <input type="text" class="form-control" name="name" placeholder="Enter your name" value="<?php echo $singleItem->name?>" required>
    </div>


        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Gardening" <?php if(in_array("Gardening",$editItem)){echo "checked";} ?> >Gardening</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Playing Football" <?php if(in_array("Playing Football",$editItem)){echo "checked";} ?>>Playing Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Coding" <?php if(in_array("Coding",$editItem)){echo "checked";} ?>>Coding</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Reading" <?php if(in_array("Reading",$editItem)){echo "checked";} ?>>Reading</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Swimming" <?php if(in_array("Swimming",$editItem)){echo "checked";} ?>>Swimming</label>
        </div>
        <input type="hidden" name="id" id="hobbyid" value="<?php echo $singleItem->id?>">
        <input type="submit">
    </form>
</div>

</body>
</html>
