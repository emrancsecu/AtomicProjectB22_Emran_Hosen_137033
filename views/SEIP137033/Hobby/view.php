<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Hobby\Hobby;
use App\Bitm\SEIP137033\Message\Message;
use App\Bitm\SEIP137033\Utility\Utility;

$hobby= new Hobby();
$hobby->prepare($_GET);
$singleItem=$hobby->view();
//var_dump($singleItem);
//die();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>View ID,Name and Hobby</h2>
    <ul class="list-group">
        <li class="list-group-item"><?php echo $singleItem->id;?></li>
         <li class="list-group-item"><?php echo $singleItem->name;?></li>
        <li class="list-group-item"><?php echo $singleItem->hobbies;?></li>
    </ul>

    <a href="index.php" class="btn btn-primary" role="button">Back To List</a>
</div>

</body>
</html>