<!DOCTYPE html>
<html lang="en">
<head>
    <title>Subcribe to newsletter</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php include_once('../../../Resources/resources.php');?>

</head>
<body>

<div class="container">
    <h2>Email Newsletter</h2>
    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label for="Title">Enter Email</label>
            <input type="Email" class="form-control" name="email" id="email" placeholder="Enter Your Email">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
