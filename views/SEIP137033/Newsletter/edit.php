<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Newsletter\Newsletter;
use App\Bitm\SEIP137033\Utility\Utility;
use App\Bitm\SEIP137033\Message\Message;

$email= new Newsletter();
$email->prepare($_GET);
//Utility::dd($email);
//die();
$singleItem=$email->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Subcribe to newsletter</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php include_once('../../../Resources/resources.php');?>

</head>
<body>

<div class="container">
    <h2>Email Newsletter</h2>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label for="Title">Enter Email</label>
            <input type="hidden" name="id" id="email" value="<?php echo $singleItem->id?>">
            <input type="Email" class="form-control" name="email" id="email" placeholder="Enter Your Email" value="<?php echo $singleItem->email ?>">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
