<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Newsletter\Newsletter;
use App\Bitm\SEIP137033\Utility\Utility;
use App\Bitm\SEIP137033\Message\Message;

$email= new Newsletter();
$allEmail=$email->trashed();
//Utility::dd($allEmail);

?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>All Trash Email List</h2>
    <form action="recoverselected.php" method="post" id="multiple">
    <a href="create.php" class="btn btn-primary" role="button">Create again</a>
    <a href="index.php" class="btn btn-primary" role="button">Back to List</a>
    <button type="submit" class="btn btn-primary">Recover Selected</button>
    <button type="button" class="btn btn-primary" id="multiple_delete">Delete Selected</button>
    <br>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Check Item</th>
                <th>Serial</th>
                <th>ID</th>
                <th>Email Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach($allEmail as $email){
                $sl++;
                ?>
                <tr>
                    <td><input type="checkbox" name="mark[]" value="<?php echo $email['id'] ?>"></td>
                    <td><?php echo $sl ?></td>
                    <td><?php echo $email['id'] ?></td>
                    <td><?php echo $email['email'] ?></td>
                    <td>
                        <a href="recover.php?id=<?php echo $email['id']?>" class="btn btn-primary" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo $email['id']?>" class="btn btn-danger" role="button" id="delete" Onclick="return ConfirmDelete()" >Delete</a>

                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        </form>
    </div>
</div>
<script>
    $('#message').show().delay(2000).fadeOut("slow");

        $(document).ready(function(){
            $("#delete").click(function(){
                if (!confirm("Do you want to delete")){
                    return false;
                }
            });
        });

        $('#multiple_delete').on('click',function(){
            document.forms[0].action="deleteselected.php";
            $('#multiple').submit();
    });
</script>
</body>
</html>
