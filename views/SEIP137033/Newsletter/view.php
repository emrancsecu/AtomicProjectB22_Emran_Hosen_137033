<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\Newsletter\Newsletter;
use App\Bitm\SEIP137033\Utility\Utility;
use App\Bitm\SEIP137033\Message\Message;

$email= new Newsletter();
$email->prepare($_GET);
//Utility::dd($email);
//die();
$singleItem=$email->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Email</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>View Email</h2>
    <ul class="list-group">
        <li class="list-group-item"><?php echo $singleItem->id;?></li>
        <li class="list-group-item"><?php echo $singleItem->email;?></li>
    </ul>

    <a href="index.php" class="btn btn-primary" role="button">Back To List</a>
</div>

</body>
</html>
