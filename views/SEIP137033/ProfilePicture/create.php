<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profile Picture</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>Create Profile</h2>
    <form role="form" action="store.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" class="form-control"name="name" required>
        </div>
        <div class="form-group">
            <label for="pwd">Upload your profile picture:</label>
            <input type="file" name="image" class="form-control" required>
        </div>
        <input type="submit" value="Submit" class="btn btn-default" role="button">
    </form>
    <br/>
    <a href="index.php" class="btn btn-primary" role="button">Back To List</a>
</div>

</body>
</html>

