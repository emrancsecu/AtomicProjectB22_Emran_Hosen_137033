<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\ProfilePicture\ImageUploader;

$profile_picture= new ImageUploader();
$single_info=$profile_picture->prepare($_GET)->view();


?>




<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profile </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>Create Profile</h2>
    <form role="form" action="update.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $single_info->id?>">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" class="form-control"name="name" value="<?php echo $single_info->name?>" >
        </div>
        <div class="form-group">
            <label for="pwd">Upload your profile picture:</label>
            <input type="file" name="image" class="form-control">
            <img src="../../../Resources/Images/<?php echo $single_info->images?>" height="100px" width="100px">
        </div>
        <input type="submit" value="Update" class="btn btn-primary">
        <br>
        <br>
        <a href="index.php" class="btn btn-info" role="button">Back</a>
    </form>
</div>

</body>
</html>

