<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\ProfilePicture\ImageUploader;
use App\Bitm\SEIP137033\Utility\Utility;
use App\Bitm\SEIP137033\Message\Message;

$profile_picture= new ImageUploader();
$allinfo=$profile_picture->trashed();




?>

<!DOCTYPE html>
<html>
<head>
    <title>All Trashed Item List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>

</head>
<body>

<div class="container">
    <h2>All Trashed Item List </h2>
    <form action="recoverselected.php" method="post" id="multiple">
    <a href="create.php" class="btn btn-primary" role="button">Create again</a>
    <a href="index.php" class="btn btn-primary" role="button">Back To list</a>
    <button type="submit" class="btn btn-primary">Recover Selected</button>
    <button type="button" class="btn btn-primary" id="multiple_delete">Delete Selected</button>
    <br/>
    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Serial</th>
                <th>ID</th>
                <th>Name</th>
                <th>Image</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allinfo as $profile_picture){
                $sl++; ?>
                <td><input type="checkbox" name="mark[]" value="<?php echo $profile_picture->id ?>"></td>
                <td><?php echo $sl?></td>
                <td><?php echo $profile_picture-> id?></td>
                <td><?php echo $profile_picture->name?></td>
                <td><img src="../../../Resources/Images/<?php echo $profile_picture->images ?>" alt="image" height="100px" width="100px" class="img-responsive"> </td>
                <td>
                    <a href="recover.php?id=<?php echo $profile_picture-> id ?>"  class="btn btn-info" role="button">Recover</a>
                    <a href="delete.php?id=<?php echo $profile_picture->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>
</div>
<script>
    $('#message').show().delay(2000).fadeOut("slow");

    $(document).ready(function(){
        $("#delete").click(function(){
            if (!confirm("Do you want to delete")){
                return false;
            }
        });
    });

    $('#multiple_delete').on('click',function(){
        document.forms[0].action="deleteselected.php";
        $('#multiple').submit();
    });

</script>

</body>
</html>
