<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP137033\ProfilePicture\ImageUploader;

$profile_picture= new ImageUploader();
$single_info=$profile_picture->prepare($_GET)->view();


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>View</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once('../../../Resources/resources.php');?>
</head>
<body>

<div class="container">
    <h2>Info</h2>
    <ul class="list-group">
        <li class="list-group-item"><b>ID:</b><?php echo $single_info->id;?></li>
        <li class="list-group-item"><b>Name:</b><?php echo $single_info->name;?></li>
        <li class="list-group-item">
            <b>Profile Picture:</b>
            <br>
            <img src="../../../Resources/Images/<?php echo $single_info->images?>" height="150px" width="150px">
        </li>
    </ul>

    <a href="index.php" class="btn btn-primary" role="button">Back To List</a>
</div>

</body>
</html>
